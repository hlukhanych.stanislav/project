﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Order obj = new Order();
            obj.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CancelTheOrder obj = new CancelTheOrder();
            obj.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Admin obj = new Admin();
            obj.ShowDialog();
        }
    }
}
