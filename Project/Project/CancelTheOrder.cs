﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    public partial class CancelTheOrder : Form
    {
        public CancelTheOrder()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex == 0)
            {
                Operations.CancelTheOrderCinema(textBox1);
            }

            else if(comboBox1.SelectedIndex == 1)
            {
                Operations.CancelTheOrderTheatre(textBox1);
            }

            else
                MessageBox.Show("Оберіть квиток");
        }
    }
}
