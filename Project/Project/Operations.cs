﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    internal class Operations
    {
        public static void WriteTheOrderTheatre(TextBox text, NumericUpDown number, TextBox phoneNumber)
        {
            StreamReader sr = new StreamReader("D:\\C#\\Курсова\\Project\\Квитки на виставу.txt");
            int tickets = Convert.ToInt32(sr.ReadLine());
            sr.Close();

            if (tickets > 0)
            {
                string name = text.Text;
                string count = number.Value.ToString();
                string phone = phoneNumber.Text;
                StreamWriter sw = File.AppendText("D:\\C#\\Курсова\\Project\\Вистава.txt");
                sw.WriteLine(name);
                sw.WriteLine(phone);
                sw.WriteLine(count);
                sw.Close();

                int num = Convert.ToInt32(number.Value);
                int res = tickets - num;
                StreamWriter SW = new StreamWriter("D:\\C#\\Курсова\\Project\\Квитки на виставу.txt");
                SW.Write(res);
                SW.Close();

                MessageBox.Show("Успішно");
            }

            else
                MessageBox.Show("Квитки закічились");
        }
        public static void WriteTheOrderCinema(TextBox text, NumericUpDown number, TextBox phoneNumber)
        {
            StreamReader sr = new StreamReader("D:\\C#\\Курсова\\Project\\Квитки на фільм.txt");
            int tickets = Convert.ToInt32(sr.ReadLine());
            sr.Close();

            if (tickets > 0)
            {
                string name = text.Text;
                string count = number.Value.ToString();
                string phone = phoneNumber.Text;
                StreamWriter sw = File.AppendText("D:\\C#\\Курсова\\Project\\Фільм.txt");
                sw.WriteLine(name);
                sw.WriteLine(phone);
                sw.WriteLine(count);
                sw.Close();

                int num = Convert.ToInt32(number.Value);
                int res = tickets - num;
                StreamWriter SW = new StreamWriter("D:\\C#\\Курсова\\Project\\Квитки на фільм.txt");
                SW.Write(res);
                SW.Close();

                MessageBox.Show("Успішно");
            }
            else
                MessageBox.Show("Білети закічились");
        }
        public static void CancelTheOrderTheatre(TextBox text)
        {
            string name = text.Text;
            List<string> list = new List<string>();

            StreamReader sr = new StreamReader("D:\\C#\\Курсова\\Project\\Вистава.txt");
            while(!sr.EndOfStream)
            {
                list.Add(sr.ReadLine());
            }
            sr.Close();

            if (list.Contains(name))
            {
                int number = list.IndexOf(name);
                int count = Convert.ToInt32(list[number + 2]);
                list.Remove(name);
                list.RemoveAt(number);
                list.RemoveAt(number);
                StreamWriter sw = new StreamWriter("D:\\C#\\Курсова\\Project\\Вистава.txt");
                for (int i = 0; i < list.Count; i++)
                {
                    sw.WriteLine(list[i]);
                }
                sw.Close();

                StreamReader SR = new StreamReader("D:\\C#\\Курсова\\Project\\Квитки на виставу.txt");
                int tickets = Convert.ToInt32(SR.ReadLine());
                SR.Close();

                int res = tickets + count;
                StreamWriter SW = new StreamWriter("D:\\C#\\Курсова\\Project\\Квитки на виставу.txt");
                SW.Write(res);
                SW.Close();

                MessageBox.Show("Успішно");
            }
            else
                MessageBox.Show("Нема замовлення на це ім'я");
        }
        public static void CancelTheOrderCinema(TextBox text)
        {
            string name = text.Text;
            List<string> list = new List<string>();

            StreamReader sr = new StreamReader("D:\\C#\\Курсова\\Project\\Фільм.txt");
            while (!sr.EndOfStream)
            {
                list.Add(sr.ReadLine());
            }
            sr.Close();

            if (list.Contains(name))
            {
                int number = list.IndexOf(name);
                int count = Convert.ToInt32(list[number + 2]);
                list.Remove(name);
                list.RemoveAt(number);
                list.RemoveAt(number);
                StreamWriter sw = new StreamWriter("D:\\C#\\Курсова\\Project\\Фільм.txt");
                for (int i = 0; i < list.Count; i++)
                {
                    sw.WriteLine(list[i]);
                }
                sw.Close();

                StreamReader SR = new StreamReader("D:\\C#\\Курсова\\Project\\Квитки на фільм.txt");
                int tickets = Convert.ToInt32(SR.ReadLine());
                SR.Close();

                int res = tickets + count;
                StreamWriter SW = new StreamWriter("D:\\C#\\Курсова\\Project\\Квитки на фільм.txt");
                SW.Write(res);
                SW.Close();

                MessageBox.Show("Успішно");
            }
            else
                MessageBox.Show("Нема замовлення на це ім'я");
        }
    }
}
