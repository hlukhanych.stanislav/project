﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;


namespace Project
{
    internal class ImportFromExcel
    {
        public static void Камінь(DataGridView grid, string fileName)
        {
            Excel.Application excelApp = new Excel.Application();

            Excel.Workbook workbook = excelApp.Workbooks.Open(fileName);

            Excel.Worksheet worksheet = (Excel.Worksheet)workbook.Sheets[1];

            Excel.Range range = worksheet.UsedRange;

            try
            {
                int rowCount = range.Rows.Count;
                int columnCount = range.Columns.Count;

                DataTable dataTable = new DataTable();

                for (int i = 1; i <= columnCount; i++)
                {
                    string columnName = Convert.ToString(range.Cells[1, i].Value);
                    dataTable.Columns.Add(columnName);
                }

                for (int row = 2; row <= rowCount; row++)
                {
                    DataRow dataRow = dataTable.NewRow();

                    for (int column = 1; column <= columnCount; column++)
                    {
                        dataRow[column - 1] = range.Cells[row, column].Value;
                    }

                    dataTable.Rows.Add(dataRow);
                }

                grid.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка при імпортуванні даних: " + ex.Message);
            }
            finally
            {
                workbook.Close();
                excelApp.Quit();
            }
        }
    }
}
