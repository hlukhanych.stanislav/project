﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    [Serializable]
    internal class OrdersList
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Count { get; set; }

        public OrdersList(string order, string phone, string count)
        {
            Name = order;
            Phone = phone;
            Count = count;
        }
    }
}
