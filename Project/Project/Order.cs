﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    public partial class Order : Form
    {
        public Order()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (numericUpDown1.Value <= Convert.ToInt32(tickets))
            {
                if (comboBox1.SelectedIndex == 0)
                {
                    Operations.WriteTheOrderCinema(textBox1, numericUpDown1, textBox2);
                }

                else if (comboBox1.SelectedIndex == 1)
                {
                    Operations.WriteTheOrderTheatre(textBox1, numericUpDown1, textBox2);
                }

                else
                    MessageBox.Show("Оберіть квиток");
            }

            else
                MessageBox.Show("Неможливо замовити таку кількість квитків");
        }

        string tickets;
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                StreamReader sr = new StreamReader("D:\\C#\\Курсова\\Project\\Квитки на фільм.txt");
                tickets = sr.ReadLine();
                sr.Close();
                label4.Text = $"Залишилось {tickets} квитків";
            }

            else if (comboBox1.SelectedIndex == 1)
            {
                StreamReader sr = new StreamReader("D:\\C#\\Курсова\\Project\\Квитки на виставу.txt");
                tickets = sr.ReadLine();
                sr.Close();
                label4.Text = $"Залишилось {tickets} квитків";
            }
        }
    }
}
