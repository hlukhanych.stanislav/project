﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExcelDataReader;

namespace Project
{
    public partial class ForTheOwner : Form
    {
        List<string> orders;
        List<OrdersList> Orders;
        BindingSource BindSource;

        public ForTheOwner()
        {
            InitializeComponent();
        }

        private void ForTheOwner_Load(object sender, EventArgs e)
        {
            orders = new List<string>();
            Orders = new List<OrdersList>();
            BindSource = new BindingSource();
        }

        private void завантажитиВDataGridViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                dataGridView1.DataSource = new object();
                dataGridView2.DataSource = new object();
                orders.Clear();
                Orders.Clear();
                BindSource.Clear();

                StreamReader sr = new StreamReader(openFileDialog1.FileName);
                while(!sr.EndOfStream)
                {
                    orders.Add(sr.ReadLine());
                }
                sr.Close();

                for (int i = 0; i < orders.Count; i++)
                {
                    Orders.Add(new OrdersList(orders[i], orders[i + 1], orders[i + 2]));
                    i++;
                    i++;
                }

                BindSource.DataSource = Orders;
                dataGridView1.DataSource = BindSource;

                dataGridView1.Columns["Name"].HeaderText = "Замовник";
                dataGridView1.Columns["Phone"].HeaderText = "Номер телефону";
                dataGridView1.Columns["Count"].HeaderText = "Кількість";
            }
        }

        private void закритиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        List<OrdersList> OrdersFiltered;
        BindingSource BindSourceFiltered;
        private void button1_Click(object sender, EventArgs e)
        {
            string name = textBox1.Text;
            OrdersFiltered = Orders.FindAll(order => order.Name.Contains(name));

            BindSourceFiltered = new BindingSource();
            BindSourceFiltered.DataSource = OrdersFiltered;

            dataGridView2.DataSource = BindSourceFiltered;

            dataGridView2.Columns["Name"].HeaderText = "Замовник";
            dataGridView2.Columns["Phone"].HeaderText = "Номер телефону";
            dataGridView2.Columns["Count"].HeaderText = "Кількість";
        }

        private void експортВExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                ExcelConverter.saveGridToExcel(dataGridView1, saveFileDialog1.FileName, "ExportData");
            }
        }

        //DataTableCollection tableCollection = null;
        private void імпортЗExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //comboBox1.Visible = true;

                //Text = openFileDialog1.FileName;

                //OpenExcelFile(openFileDialog1.FileName);

                ImportFromExcel.Камінь(dataGridView1, openFileDialog1.FileName);

                //StreamReader sr = new StreamReader(openFileDialog1.FileName);

                //orders.Clear();
                //Orders.Clear();
                //BindSource.Clear();

                //while (!sr.EndOfStream)
                //{
                //    orders.Add(Convert.ToString(sr.Read()));
                //}

                //sr.Close();

                //for (int i = 0; i < orders.Count - 2; i++)
                //{
                //    Orders.Add(new OrdersList(orders[i], orders[i + 1], orders[i + 2]));
                //    i++;
                //    i++;
                //}

                //BindSource.DataSource = Orders;
                //dataGridView1.DataSource = BindSource;

                //dataGridView1.Columns["Name"].HeaderText = "Замовник";
                //dataGridView1.Columns["Phone"].HeaderText = "Номер телефону";
                //dataGridView1.Columns["Count"].HeaderText = "Кількість";

                //FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                //BinaryFormatter bf = new BinaryFormatter();

                //BindSource.Clear();

                //foreach (OrdersList b in (List<OrdersList>)bf.Deserialize(fs))
                //    BindSource.Add(b);

                //fs.Close();
            }
        }
        //private void OpenExcelFile(string path)
        //{
        //    FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read);

        //    IExcelDataReader r = ExcelReaderFactory.CreateReader(fs);

        //    DataSet ds = r.AsDataSet(new ExcelDataSetConfiguration()
        //    {
        //        ConfigureDataTable = (x) => new ExcelDataTableConfiguration()
        //        {
        //            UseHeaderRow = false
        //        }
        //    });

        //    tableCollection = ds.Tables;

        //    comboBox1.Items.Clear();

        //    foreach (DataTable tabe in tableCollection)
        //    {
        //        comboBox1.Items.Add(tabe.TableName);
        //    }

        //    comboBox1.SelectedIndex = 0;
        //}

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataTable table = tableCollection[Convert.ToString(comboBox1.SelectedItem)];

            //dataGridView1.DataSource = table;
        }
    }
}
