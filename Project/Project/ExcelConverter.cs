﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Project
{
    internal class ExcelConverter
    {
        public static void saveGridToExcel(DataGridView grid, string fileName, string sheetName)
        {
            Excel.Application excelApp = new Excel.Application();
            
            excelApp.Visible = true;
            
            excelApp.SheetsInNewWorkbook = 1;
            
            Excel.Workbook workBook = excelApp.Workbooks.Add(Type.Missing);
            
            excelApp.DisplayAlerts = false;
            
            Excel.Worksheet sheet = (Excel.Worksheet)excelApp.Worksheets.get_Item(1);
            
            sheet.Name = sheetName;

            int offsetRow = 1;
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                sheet.Cells[offsetRow, i + 1] = grid.Columns[i].HeaderText;
            }

            for (int i = 0; i < grid.RowCount; i++)
            {
                for (int j = 0; j < grid.ColumnCount; j++)
                {
                    sheet.Cells[offsetRow + i + 1, j + 1] = grid[j, i].Value;
                }
            }

            Excel.Range range = sheet.Range[sheet.Cells[offsetRow, 1], sheet.Cells[offsetRow, grid.ColumnCount]];
            range.Cells.Font.Name = "Arial";
            range.Cells.Font.Size = 12;

            excelApp.Application.ActiveWorkbook.SaveAs($"{fileName}.xlsx");
        }
    }
}
